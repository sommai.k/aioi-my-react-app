import { createBrowserRouter } from "react-router-dom";
import { Login } from "./components/page/Login";
import { Home } from "./components/page/Home";
import { Layout } from "./components/page/Layout";
import { Todo } from "./components/page/Todo";
import { TestInput } from "./components/page/TestInput";
import { NoPage } from "./components/page/NoPage";
import { App } from "./App";
import { User } from "./components/page/User";
export const router = createBrowserRouter([
    {
        path: 'login',
        element: <Login />
    },
    // {
    //     path: '*',
    //     element: <NoPage />,
    // },
    {
        path: 'app',
        element: <App />
    },
    {
        path: '/',
        element: <Layout />,
        errorElement: <NoPage />,
        children: [
            {
                path: 'home',
                element: <Home />,
            },
            {
                path: 'todo',
                element: <Todo />,
            },
            {
                path: 'testinput',
                element: <TestInput />
            },
            {
                path: 'user',
                element: <User />
            },
        ]
    },
]);