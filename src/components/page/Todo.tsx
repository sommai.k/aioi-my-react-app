/**
 * Author : Jakkit.N
 */
import { zodResolver } from '@hookform/resolvers/zod';
import { FC, useCallback, useReducer, useState } from 'react';
import { useForm, Controller, SubmitErrorHandler } from 'react-hook-form';
import { z } from 'zod';

interface Todo {
    code: string;
}

type TodoAction = { type: string; payload: Todo }


const reducer = (state: Todo[], action: TodoAction) => {
    let data = state;
    switch (action.type) {
        case 'ADD':
            data = [...state, action.payload];
            break;
        case 'DELETE':
            data = state.filter(todo => todo.code !== action.payload.code);
            break;
    }
    return data;
}
// Declare Schema
const FormSchema = z.object({
    code: z.string().min(3, "กรุณาระบุข้อความขั้นต่ำ 3 ตัวอักษร")
});

type FormModel = z.infer<typeof FormSchema>;

// Fuction Component
export const Todo: FC = () => {
    const { control, getValues, handleSubmit } = useForm<FormModel>({
        defaultValues: {
            code: ''
        },
        resolver: zodResolver(FormSchema)
    });
    const [error, setError] = useState("");
    const [todos, dispatch] = useReducer(reducer, []);

    const addTodo = useCallback(() => {
        const inputValue = getValues('code');

        dispatch({ type: 'ADD', payload: { code: inputValue } });
    }, [todos]);

    const delTodo = useCallback((code: string) => {
        dispatch({ type: 'DELETE', payload: { code: code } });
    }, [todos]);

    const whenAddError: SubmitErrorHandler<FormModel> = (error) => {
        if (error.code?.message) {
            setError(error.code?.message);
        }
    }

    return <>
        <div>
            <label data-testid="code-label">Code</label>
            <Controller control={control} name="code" render={(r) =>
                <input className='border border-blue-500' data-testid="code-input" {...r.field} />
            } />
            <button className='btn-primary' data-testid="add-btn" onClick={handleSubmit(addTodo, whenAddError)}>
                ADD
            </button>
            <div data-testid="error-msg">{error}</div>
        </div>
        <div>
            <table>
                <thead>
                    <tr>
                        <th data-testid="code-th">Code</th>
                        <th data-testid="del-th">Del</th>
                    </tr>
                </thead>
                <tbody>
                    {todos.map((todo, index) => (
                        <tr key={index}>
                            <td data-testid="code-td">{todo.code}</td>
                            <td>
                                <button data-testid="del-btn" className='btn-primary' onClick={() => delTodo(todo.code)} >Delete</button>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    </>
}