import { FC, useState } from 'react';
import { NavLink } from 'react-router-dom';
import { classNames } from '../ui/util';

interface MenuItem {
    path: string;
    label: string;
}

const initMenu: MenuItem[] = [
    {
        path: '/home', label: 'Home'
    },
    {
        path: '/todo', label: 'Todo'
    },
    {
        path: '/testinput', label: 'Test Input'
    },
    {
        path: '/user', label: 'User Setup'
    },
]

export const Menu: FC = () => {
    const [menu] = useState<MenuItem[]>(initMenu);

    return <>
        <ul className='w-full flex flex-col'>
            {
                menu.map((menu) =>
                    <li key={menu.path} className='flex w-full'>
                        <NavLink
                            to={menu.path}
                            className={(n) => classNames("w-full px-4 py-2", n.isActive ? "bg-blue-400" : "hover:bg-blue-300")}
                        >{menu.label}</NavLink>
                    </li>
                )
            }
        </ul>
    </>
}