import { FC, useState, useEffect } from 'react';
import { PlusCircleIcon, PencilIcon, TrashIcon } from '@heroicons/react/24/solid'
import { XCircleIcon } from '@heroicons/react/24/outline'
import { api } from '../../app/api';
import { AlertModal, AIOIProgress, ConfirmModal } from '../ui';
import { useAlert, useConfirm, useProgress } from '../../hooks';
import { UserResp } from '../../model/User';
import { UserForm, UserModel } from './UserForm';

export const User: FC = () => {
    // hooks
    const [userData, setUserData] = useState([]);
    const [showForm, setShowForm] = useState(false);
    const [initUser, setInitUser] = useState<UserModel>();
    const [mode, setMode] = useState<'add' | 'edit'>('add');
    const alert = useAlert();
    const confirm = useConfirm();
    const progress = useProgress();

    // custom function
    const loadUserData = async () => {
        try {
            progress.show();
            const resp = await api.get<UserResp>('/api/users?limit=10&offset=0');
            if (resp.data.success) {
                console.log(resp.data.data);
                setUserData(resp.data.data);
            } else {
                alert.error("Server error");
            }
        } catch (e) {
            alert.error("Server error");
        } finally {
            progress.hide();
        }
    }

    const whenSave = async (user: any) => {
        try {
            progress.show();
            if (mode === 'add') {
                const resp = await api.post('/api/users', user);
                if (resp.status === 201) {
                    setShowForm(false);
                    loadUserData();
                    alert.info("Save success");
                } else {
                    alert.warning("Save fail please try again");
                }
            } else if (mode === 'edit') {
                const resp = await api.put(`/api/users/${user.id}`, user);
                if (resp.status === 200) {
                    setShowForm(false);
                    loadUserData();
                    alert.info("Update success");
                } else {
                    alert.warning("Update fail please try again");
                }
            } else {
                // show warning
            }
        } catch (e) {
            alert.error("Server error");
        } finally {
            progress.hide();
        }
    }

    const whenAdd = () => {
        setMode('add');
        setShowForm(true);
    }

    const whenDelete = async (id: number) => {
        confirm.show("Confirm", "Please confirm to delete", async () => {
            await api.delete(`/api/users/${id}`);
            confirm.hide();
            loadUserData();
        }, () => {
            // when cancel click
            confirm.hide();
        });
    }

    const whenEdit = async (id: number) => {
        try {
            setMode('edit');
            const resp = await api.get(`/api/users/${id}`);
            if (resp.status === 200) {
                setInitUser({ ...resp.data });
                setShowForm(true);
            }
        } catch (e) {

        }
    }

    useEffect(() => {
        loadUserData();
    }, [])
    // render
    return <>
        <div className='flex flex-col w-full p-2 space-y-2'>
            <div>
                <button data-testid="add-btn" onClick={whenAdd}
                    className='flex flex-row space-x-1 hover:border-blue-800 border p-2 rounded-md'>
                    <PlusCircleIcon className='w-6 h-6 text-green-500' /><span>Add</span>
                </button>
            </div>
            <div className='border rounded-lg min-h-[300px]'>
                <table className='w-full'>
                    <thead className='bg-blue-800 text-white'>
                        <tr>
                            <th data-testid="id-th" className='p-2 w-[80px]'>Id</th>
                            <th data-testid="userName-th" className='p-2'>User Name</th>
                            <th data-testid="name-th" className='p-2'>Name</th>
                            <th data-testid="action-th" className='p-2 w-[150px]'></th>
                        </tr>
                    </thead>
                    <tbody>
                        {!progress.isShow &&
                            userData.map((user) => (
                                <tr className='hover:bg-blue-50 even:bg-gray-50' key={user["id"]}>
                                    <td data-testid="id-td" className='text-center border p-2'>{user["id"]}</td>
                                    <td data-testid="userName-td" className='border p-2'>{user["userName"]}</td>
                                    <td data-testid="name-td" className='border p-2'>{`${user["firstName"]} ${user["lastName"]}`}</td>
                                    <td data-testid="action-td"
                                        className='flex flex-row space-x-5 justify-center border p-2'>
                                        <PencilIcon
                                            data-testid="edit-td"
                                            className='w-6 h-6 text-yellow-500'
                                            onClick={() => whenEdit(user["id"])}
                                        />
                                        <TrashIcon data-testid="delete-td" className='w-6 h-6 text-red-500' onClick={() => whenDelete(user["id"])} />
                                    </td>
                                </tr>
                            )
                            )
                        }
                    </tbody>
                </table>
            </div>
        </div>
        <AlertModal testid='alert' type='warning' {...alert.model} whenClose={alert.hide} />
        <ConfirmModal testid='confirm' {...confirm.model} whenConfirm={confirm.model.whenOk} whenCancel={confirm.model.whenCancel} />
        <AIOIProgress {...progress} />
        {showForm &&
            <div className='absolute top-0 left-0 w-full h-full flex flex-col space-y-4 z-10 bg-gray-500 bg-opacity-60 items-center justify-center'>
                <div className='w-[400px] space-y-3 p-4 bg-white rounded-lg' >
                    <div className='flex flex-row'>
                        <div className='text-2xl flex-1'>User Form</div>
                        <div>
                            <XCircleIcon className='h-8 w-8 right-0 hover:text-gray-600 hover:cursor-pointer' onClick={() => setShowForm(false)} />
                        </div>
                    </div>
                    <UserForm whenSave={whenSave} initUser={initUser} />
                </div>
            </div>
        }
    </>
}