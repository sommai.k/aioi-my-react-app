import { FC } from 'react';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { z } from 'zod';
import { useForm, SubmitHandler } from 'react-hook-form';
import { zodResolver } from '@hookform/resolvers/zod';

import { login } from '../../app/features/auth-slice';
import { AppDispatch } from '../../app/store';
import { api } from '../../app/api';
import { AlertModal, AIOIInput, AIOIButton, AIOIProgress } from '../ui';
import { useProgress, useAlert } from '../../hooks';
import { classNames } from '../ui/util';

const LoginSchema = z.object({
    userName: z.string().min(5, "Username is required").max(25, "Username must be <= 25"),
    password: z.string().min(8, "Password is required").max(25, "Password must be <= 25"),
});

type LoginModel = z.infer<typeof LoginSchema>;

interface LoginResponse {
    success: boolean;
    token: string;
    userName: string;
    firstName: string;
    lastName: string;
}

export const Login: FC = () => {
    // begin Hook
    const dispatch = useDispatch<AppDispatch>();
    const { control, handleSubmit, reset } = useForm<LoginModel>({
        defaultValues: {
            userName: '',
            password: ''
        },
        resolver: zodResolver(LoginSchema)
    });

    const navigate = useNavigate();
    const progress = useProgress();
    const alert = useAlert();
    // end hook

    // begin custom function
    const loginSuccess = async (user: LoginModel) => {
        try {
            progress.show();
            const resp = await api.post<LoginResponse>("/api/login", user);
            const { token, userName, firstName, lastName } = resp.data;
            if (resp.data.success) {
                dispatch(login({
                    token,
                    userName,
                    firstName,
                    lastName
                }));
                navigate("/home");
            } else {
                // show warning message box
                alert.warning('Invalid username or password');
            }
        } catch (e) {
            // show error message box
            alert.error('Server Error');
        } finally {
            progress.hide();
        }
    }

    const whenValidateSuccess: SubmitHandler<LoginModel> = (login) => {
        loginSuccess(login)
    }

    const whenClose = () => {
        // setAlert(alertInitState);
        alert.hide();
    }
    // end custom function

    // renderer
    return <>
        <div className='h-screen w-screen bg-gradient-to-br from-[#0667a8] to-[#d2edfb] flex flex-col'>
            <div className='p-8 space-y-4 md:w-1/3 md:m-auto shadow-2xl rounded-lg border-blue-200 bg-gray-50 border'>
                <div className={
                    classNames(
                        'text-blue-800 md:text-green-800 lg:text-red-800 xl:text-yellow-800',
                        'text-3xl font-semibold tracking-wide'
                    )
                }>Welcome to system</div>
                <AIOIInput testid="user" control={control} label='Username' name="userName" />
                <AIOIInput testid="password" control={control} label='Password' name="password" isSecure={true} />
                <div className='flex flex-row space-x-2'>
                    <AIOIButton testid="login-btn" whenClick={handleSubmit(whenValidateSuccess)} label='Login' />
                    <AIOIButton testid="reset-btn" whenClick={() => reset()} label='Reset' />
                </div>
            </div>
        </div>
        <AlertModal testid='alert' type='warning' {...alert.model} whenClose={whenClose} />
        <AIOIProgress isShow={progress.isShow} />
    </>
}