import { FC, useEffect, useState } from 'react';
import { Outlet, Navigate } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from '../../app/store';
import { Menu } from './Menu';
import { Header } from './Header';
import { login } from '../../app/features/auth-slice';
export const Layout: FC = () => {
    const isLogedIn = useSelector((state: RootState) => state.authReducer.isLogedIn);
    const dispatch = useDispatch();
    const [ready, setReady] = useState(false);

    useEffect(() => {
        const auth = localStorage.getItem('auth');
        if (auth) {
            dispatch(login(JSON.parse(auth)));
        }
        setReady(true);
    }, []);

    return <>
        {ready &&
            <div className='w-screen h-screen flex flex-col'>
                <div className='w-full h-24 shadow-md'>
                    <Header />
                </div>
                <div className='flex flex-row flex-1 '>
                    <div className='min-w-52 h-full'>
                        <Menu />
                    </div>
                    <div className='flex-1 flex flex-col h-full relative'>
                        <div className='w-full h-full'>{isLogedIn ? <Outlet /> : <Navigate to="/login" replace={true} />}</div>
                        <div className='absolute bottom-0 h-20 shadow-inner w-full text-indigo-600 text-center pt-4'>Footer</div>
                    </div>
                </div>
            </div >
        }
    </>
}