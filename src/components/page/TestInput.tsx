import { FC, useState, useRef, useEffect } from 'react';

export const TestInput: FC = () => {
    const [inpValue, setInpValue] = useState("");
    const inp = useRef<HTMLInputElement>(null);
    const showValue = () => {
        console.log("#input 1 = ", inpValue, "#input 2 = ", inp.current?.value);
    }
    useEffect(() => {
        console.log("Render TestInput");
    });

    return <>
        <div>
            <input data-testid="01-input" className='border border-blue-500' type="text" onChange={(e) => setInpValue(e.target.value)} />
            <button data-testid="01-btn" className='btn-primary' onClick={showValue}>Test Show Value</button>
        </div>
        <div>
            <input data-testid="02-input" className='border border-blue-500' type="text" ref={inp} />
            <button data-testid="02-btn" className='btn-primary' onClick={showValue}>Test Show Value</button>
        </div>
    </>
}