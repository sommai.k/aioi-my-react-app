import { FC, useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { RootState } from '../../app/store';
import { api } from '../../app/api';
import { UserResp } from '../../model/User';


export const Home: FC = () => {
    const auth = useSelector((state: RootState) => state.authReducer);
    const [users, setUsers] = useState([]);

    // custom function
    const loadUser = async () => {
        const resp = await api.get<UserResp>('/api/users/all');
        if (resp.data.success) {
            setUsers(resp.data.data);
        } else {
            // show warning alert
        }
    }
    // onLoad
    useEffect(() => {
        loadUser();
    }, []);

    return <>
        <div data-testid="display">{`${auth.firstName} ${auth.lastName} (${auth.userName})`}</div>
        <ul>
            {users &&
                users.map((user: any) =>
                    <li key={user.id}>{user.id} {user.userName}</li>
                )
            }
        </ul>
    </>
}