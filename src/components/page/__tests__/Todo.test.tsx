import { describe, test, expect } from 'vitest';
import { render, act } from '@testing-library/react';
import { Todo } from '../Todo';
import "@testing-library/jest-dom";
import userEvent from '@testing-library/user-event';

describe("Todo", () => {
    const user = userEvent.setup();

    test("Should render", async () => {
        const { getByTestId, unmount } = render(<Todo />);
        // assert form
        const label = getByTestId("code-label");
        const code = getByTestId("code-input");
        const btn = getByTestId("add-btn");
        expect(label.textContent).toBe("Code");
        expect(code).toBeInTheDocument();
        expect(btn.textContent).toBe("ADD");

        // assert table
        const codeTh = getByTestId("code-th");
        const delTh = getByTestId("del-th");
        expect(codeTh.textContent).toBe("Code");
        expect(delTh.textContent).toBe("Del");
        unmount();
    });

    test("Should add new item", async () => {
        const { getByTestId, unmount, findAllByTestId } = render(<Todo />);
        const code = getByTestId("code-input");
        const btn = getByTestId("add-btn");
        // user action
        await act(() => user.type(code, "001"));
        await act(() => user.click(btn));

        // assert 
        const codeTd = await findAllByTestId("code-td");
        expect(codeTd).toHaveLength(1);
        expect(codeTd[0].textContent).toBe("001");
        unmount();
    });

    test("Should delete item", async () => {
        const { getByTestId, unmount, findAllByTestId } = render(<Todo />);
        const code = getByTestId("code-input");
        const btn = getByTestId("add-btn");
        // user action
        await act(() => user.type(code, "001"));
        await act(() => user.click(btn));
        await act(() => user.type(code, "002"));
        await act(() => user.click(btn));
        // assert 
        const codeTd = await findAllByTestId("code-td");
        expect(codeTd).toHaveLength(2);
        const delBtn = await findAllByTestId("del-btn");
        await act(() => user.click(delBtn[0]));
        const newCodeTd = await findAllByTestId("code-td");
        expect(newCodeTd).toHaveLength(1);
        unmount();
    });

    test("Should show waring when code is null", async () => {
        const { unmount, getByTestId } = render(<Todo />);
        const addBtn = getByTestId("add-btn");
        await act(() => user.click(addBtn));
        const errorMsg = getByTestId("error-msg");
        expect(errorMsg.textContent).toBe("กรุณาระบุข้อความขั้นต่ำ 3 ตัวอักษร");
        unmount();
    });
});