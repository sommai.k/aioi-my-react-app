import { describe, test, expect, beforeAll, afterAll, afterEach, vitest } from 'vitest';
import { renderWithProviders } from '../../../../utils/test-utils';
import { User } from '../User';
import "@testing-library/jest-dom";
import { setupServer } from 'msw/node';
import { HttpResponse, http } from 'msw';
import { api } from '../../../app/api';

describe('User', () => {
    const server = setupServer(
        http.get("/api/users", () => HttpResponse.json(
            {
                "success": true,
                "data": [{
                    "id": 1,
                    "userName": "admin",
                    "firstName": "test",
                    "lastName": "system"
                }],
                "total": 1
            }
        ))
    );

    beforeAll(() => {
        server.listen();
    });

    afterAll(() => {
        server.close();
    });

    afterEach(() => {
        server.resetHandlers();
    })

    test("Should render", async () => {
        const { unmount, getByTestId, } = renderWithProviders(<User />);
        // begin test case

        const addBtn = getByTestId('add-btn');
        const idTh = getByTestId('id-th');
        const userNameTh = getByTestId('userName-th');
        const nameTh = getByTestId('name-th');
        const actionTh = getByTestId('action-th');

        expect(addBtn).toHaveTextContent('Add');
        expect(idTh).toHaveTextContent('Id');
        expect(userNameTh).toHaveTextContent('User Name');
        expect(nameTh).toHaveTextContent('Name');
        expect(actionTh).toHaveTextContent('');

        // end test case
        unmount();
    });

    test("Should show user data", async () => {
        const apiSpy = vitest.spyOn(api, 'get');
        const { unmount, getByTestId } = renderWithProviders(<User />);
        // begin test case
        const idTd = getByTestId('id-td');
        const userNameTd = getByTestId('userName-td');
        const nameTd = getByTestId('name-td');
        const editTd = getByTestId('edit-td');
        const deleteTd = getByTestId('delete-td');
        expect(idTd).toHaveTextContent('1');
        expect(userNameTd).toHaveTextContent('admin');
        expect(nameTd).toHaveTextContent('test system');
        expect(editTd).toBeInTheDocument();
        expect(deleteTd).toBeInTheDocument();
        expect(apiSpy).toBeCalled();
        expect(apiSpy).toBeCalledWith('/api/users');
        // end test case
        unmount();
    });
});