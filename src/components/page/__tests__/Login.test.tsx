import { describe, test, expect, beforeAll, afterAll, afterEach } from 'vitest';
import { renderWithProviders } from '../../../../utils/test-utils';
import { Login } from '../Login';
import userEvent from '@testing-library/user-event';
import { setupServer } from 'msw/node';
import { HttpResponse, http } from 'msw';
import "@testing-library/jest-dom";

describe("Login", () => {
    const user = userEvent.setup();

    const server = setupServer(

        http.post("/api/login", () => {
            return HttpResponse.json({
                success: true,
                userCode: "admin",
                firstName: "Administrator",
                lastName: "XX",
                token: ""
            })
        }),

    );

    beforeAll(() => {
        server.listen();
    });

    afterAll(() => {
        server.close();
    });

    afterEach(() => {
        server.resetHandlers();
    });

    test("Should render", async () => {
        const { unmount, getByTestId, store } = renderWithProviders(<Login />);
        // begin testcase
        const userLabel = getByTestId("user-label");
        const userInput = getByTestId("user-input");
        const passwordLabel = getByTestId("password-label");
        const passwordInput = getByTestId("password-input");
        const loginBtn = getByTestId("login-btn");
        const resetBtn = getByTestId("reset-btn");

        expect(userLabel.textContent).toBe("Username");
        expect(userInput).toHaveProperty("value", "");
        expect(passwordLabel.textContent).toBe("Password");
        expect(passwordInput).toHaveProperty("type", "password");
        expect(passwordInput).toHaveProperty("value", "");
        expect(loginBtn.textContent).toBe("Login");
        expect(resetBtn.textContent).toBe("Reset");
        expect(store.getState().authReducer.isLogedIn).toBeFalsy();

        // end testcase
        unmount();
    });

    test("Should validate pass", async () => {
        const { unmount, getByTestId } = renderWithProviders(<Login />);
        // begin testcase
        const userInput = getByTestId("user-input");
        const passwordInput = getByTestId("password-input");
        const loginBtn = getByTestId("login-btn");

        await user.type(userInput, "admin");
        await user.type(passwordInput, "1234");
        await user.click(loginBtn);

        expect(userInput).toHaveValue("admin");
        expect(passwordInput).toHaveValue("1234");

        // end testcase
        unmount();
    });

    test("Should validate fail", async () => {
        const { unmount, getByTestId } = renderWithProviders(<Login />);
        // begin testcase
        const userInput = getByTestId("user-input");
        const passwordInput = getByTestId("password-input");
        const loginBtn = getByTestId("login-btn");

        await user.click(loginBtn);
        expect(getByTestId("user-error")).toHaveTextContent(/Username/);
        expect(getByTestId("password-error")).toHaveTextContent(/Password/);

        await user.type(userInput, "admin");
        await user.click(loginBtn);
        expect(getByTestId("password-error")).toHaveTextContent(/Password/);

        await user.clear(userInput);
        await user.type(passwordInput, '1234');
        await user.click(loginBtn);
        expect(getByTestId("user-error")).toHaveTextContent(/Username/);
        // end testcase
        unmount();
    });

    test("Should login success", async () => {
        const { unmount, getByTestId, store } = renderWithProviders(<Login />);
        // begin testcase
        expect(store.getState().authReducer.isLogedIn).toBeFalsy();
        const userInput = getByTestId("user-input");
        const passwordInput = getByTestId("password-input");
        const loginBtn = getByTestId("login-btn");

        await user.type(userInput, "admin");
        await user.type(passwordInput, '1234');
        await user.click(loginBtn);

        expect(store.getState().authReducer.isLogedIn).toBeTruthy();
        // end testcase
        unmount();
    });

    test("Should login fail invaid user or password", async () => {
        const restHandlers = [
            http.post("/api/login", async () => {
                return HttpResponse.json({
                    success: false,
                    message: "Invalid user or password"
                });
            }),
        ];

        server.use(...restHandlers);
        const { unmount, getByTestId, store } = renderWithProviders(<Login />);
        // begin testcase
        expect(store.getState().authReducer.isLogedIn).toBeFalsy();
        const userInput = getByTestId("user-input");
        const passwordInput = getByTestId("password-input");
        const loginBtn = getByTestId("login-btn");

        await user.type(userInput, "admin");
        await user.type(passwordInput, '1234');
        await user.click(loginBtn);

        expect(getByTestId("alert-title")).toHaveTextContent("Warning")
        expect(getByTestId("alert-message")).toHaveTextContent("Invalid username or password");

        expect(store.getState().authReducer.isLogedIn).toBeFalsy();
        // end testcase
        unmount();
    });

    test("Should login fail server error", async () => {
        const restHandlers = [
            http.post("/api/login", async () => {
                return HttpResponse.error()
            }),
        ];

        server.use(...restHandlers);
        const { unmount, getByTestId, store } = renderWithProviders(<Login />);
        // begin testcase
        expect(store.getState().authReducer.isLogedIn).toBeFalsy();
        const userInput = getByTestId("user-input");
        const passwordInput = getByTestId("password-input");
        const loginBtn = getByTestId("login-btn");

        await user.type(userInput, "admin");
        await user.type(passwordInput, '1234');
        await user.click(loginBtn);

        expect(getByTestId("alert-title")).toHaveTextContent("Error")
        expect(getByTestId("alert-message")).toHaveTextContent("Server Error");

        expect(store.getState().authReducer.isLogedIn).toBeFalsy();
        // end testcase
        unmount();
    });

    test("Should reset form", async () => {
        const { unmount, getByTestId } = renderWithProviders(<Login />);
        // begin testcase
        const userInput = getByTestId("user-input");
        const passwordInput = getByTestId("password-input");
        const resetBtn = getByTestId("reset-btn");

        await user.type(userInput, "admin");
        await user.type(passwordInput, '1234');
        await user.click(resetBtn);
        expect(userInput).toHaveValue("");
        expect(passwordInput).toHaveValue("");
        // end testcase
        unmount();
    })
});