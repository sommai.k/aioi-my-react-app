import { describe, test, expect, vi } from 'vitest';
import { render, act } from '@testing-library/react';
import { TestInput } from '../TestInput';
import "@testing-library/jest-dom";
import userEvent from '@testing-library/user-event';

describe("TestInput", () => {

    test("Should render", async () => {
        const log = vi.spyOn(console, 'log');
        const user = userEvent.setup();
        const { unmount, findAllByRole } = render(<TestInput />);
        expect(log).toBeCalledTimes(1);
        expect(log).toBeCalledWith("Render TestInput");
        const btn = await findAllByRole('button');
        await act(() => user.click(btn[0]));
        expect(log).toHaveBeenCalledWith("#input 1 = ", "", "#input 2 = ", "");
        unmount();
    });

    test("Should render with input #1 value", async () => {
        const log = vi.spyOn(console, 'log');
        const user = userEvent.setup();
        const { unmount, getByTestId } = render(
            <TestInput />
        );
        const input01 = getByTestId('01-input');
        const btn = getByTestId("01-btn");
        await act(() => user.type(input01, "abcdefg"));
        await act(() => user.click(btn));
        expect(log).toHaveBeenCalledTimes(9);
        expect(log).toHaveBeenCalledWith("#input 1 = ", "abcdefg", "#input 2 = ", "");
        unmount();
    });

    test("Should render with input #2 value", async () => {
        const log = vi.spyOn(console, 'log');
        const user = userEvent.setup();
        const { unmount, getByTestId } = render(
            <TestInput />
        );
        const input02 = getByTestId('02-input');
        const btn = getByTestId("02-btn");
        await act(() => user.type(input02, "abcdefg"));
        await act(() => user.click(btn));
        expect(log).toHaveBeenCalledTimes(2);
        expect(log).toHaveBeenCalledWith("#input 1 = ", "", "#input 2 = ", "abcdefg");
        unmount();
    });
});