import { describe, test, expect } from 'vitest';
import { renderWithProviders } from '../../../../utils/test-utils';
import { Home } from '../Home';
import "@testing-library/jest-dom";

describe("Home", () => {

    test("Should show Please Login", () => {
        const { unmount, getByText } = renderWithProviders(<Home />);
        // begin testcase
        expect(getByText("Please Login")).toBeInTheDocument();
        // end testcase
        unmount();
    });

    test("Should show Username", async () => {
        const { unmount, getByText, queryByText, getByTestId } = renderWithProviders(<Home />, {
            preloadedState: {
                authReducer: {
                    isLogedIn: true,
                    userName: "admin",
                    firstName: "Sommai",
                    lastName: "K",
                    token: "XXXXXXXX"
                }
            }
        });
        // begin testcase
        expect(getByText(/admin/)).toBeInTheDocument();
        expect(getByTestId('display').textContent).toBe("Sommai K (admin)");
        expect(queryByText("Please Login")).not.toBeInTheDocument();
        // end testcase
        unmount();
    });

});