import { FC } from 'react';
import { z } from 'zod';
import { SubmitHandler, useForm } from 'react-hook-form';
import { zodResolver } from '@hookform/resolvers/zod';
import { AIOIButton, AIOIInput } from '../ui';

const UserSchema = z.object({
    id: z.number().nullish(),
    userName: z.string().min(5).max(150),
    firstName: z.string().min(1).max(150),
    lastName: z.string().min(1).max(150),
    password: z.string().min(8).max(25),
});

export type UserModel = z.infer<typeof UserSchema>;

interface Props {
    initUser?: UserModel,
    whenSave: (user: UserModel) => void;
}

export const UserForm: FC<Props> = (props) => {
    // hooks
    const { control, handleSubmit, reset } = useForm<UserModel>({
        resolver: zodResolver(UserSchema),
        defaultValues: props.initUser ?? {
            id: null,
            userName: '',
            firstName: '',
            lastName: '',
            password: ''
        }
    });
    // custom function
    const whenValidatePass: SubmitHandler<UserModel> = async (user) => {
        // post to /users
        props.whenSave(user);
    }

    const whenReset = () => {
        reset();
    }

    return <>
        <div className='flex flex-col w-full space-y-3'>
            <AIOIInput control={control} name='userName' testid='userName' label='User Name' />
            <AIOIInput control={control} name='firstName' testid='firstName' label='First Name' />
            <AIOIInput control={control} name='lastName' testid='lastName' label='Last Name' />
            <AIOIInput control={control} name='password' testid='password' label='Password' isSecure={true} />
            <div className='flex flex-row w-full space-x-2'>
                <AIOIButton testid='save-btn' label='Save' whenClick={handleSubmit(whenValidatePass)} />
                <AIOIButton testid='reset-btn' label='Reset' whenClick={whenReset} />
            </div>
        </div>
    </>
}