import { FC } from 'react';

export const NoPage: FC = () => {
    return <>
        <div>404 Page not found</div>
    </>
}