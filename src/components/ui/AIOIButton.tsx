import { FC } from 'react';

interface Props {
    testid: string;
    label: string;
    whenClick: () => void;
}

export const AIOIButton: FC<Props> = (props) => {
    return <>
        <button
            className='btn-primary'
            data-testid={props.testid}
            onClick={props.whenClick}>{props.label}</button>
    </>
}