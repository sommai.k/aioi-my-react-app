import { FC } from 'react';
import { Control, Controller } from 'react-hook-form';
import { classNames } from './util';

interface Props {
    testid: string;
    label: string;
    name: string;
    control: Control<any>;
    isSecure?: boolean;
}

export const AIOIInput: FC<Props> = (props) => {
    return <>
        <div className='flex flex-col'>
            <label data-testid={`${props.testid}-label`} htmlFor={props.name}>{props.label}</label>
            <Controller control={props.control} name={props.name} render={(c) =>
                <>
                    <input
                        id={props.name}
                        className={
                            classNames(
                                'border px-2 py-2 shadow-sm rounded-md',
                                c.fieldState.invalid ? 'border-red-800 bg-red-100 ' : 'border-blue-800'
                            )
                        }
                        type={(props.isSecure ?? false) ? 'password' : 'text'}
                        data-testid={`${props.testid}-input`}
                        {...c.field}
                    />
                    {c.fieldState.error &&
                        <div data-testid={`${props.testid}-error`} className='text-red-500 text-sm'>{c.fieldState.error?.message}</div>
                    }
                </>
            } />
        </div>
    </>
}