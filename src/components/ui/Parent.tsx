import { Component, ReactNode } from 'react';

interface Props {
    children: ReactNode
}

export class Parent extends Component<Props> {
    render(): ReactNode {
        return <div>
            {this.props.children}
        </div>
    }
}