import { describe, test, expect, vi } from 'vitest';
import { AlertModal } from '../AlertModal';
import { render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

describe("AlertModal", () => {

    const user = userEvent.setup();

    test("Should render", async () => {
        const mockOnClose = vi.fn();
        const { getByTestId, unmount } = render(
            <AlertModal
                title="Warning"
                type='warning'
                message='Test Warning'
                isShow={true}
                whenClose={mockOnClose}
                testid='alert'
            />
        );
        expect(getByTestId('alert-title').textContent).toBe("Warning");
        expect(getByTestId('alert-message').textContent).toMatch(/Test/);
        const closeBtn = getByTestId('alert-btn');
        await user.click(closeBtn);
        expect(mockOnClose).toBeCalled();
        unmount();
    });

});