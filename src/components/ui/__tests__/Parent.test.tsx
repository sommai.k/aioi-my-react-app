import { describe, test, expect } from 'vitest';
import { Parent } from '../Parent';
import { BigText } from '../BigText';
import { render } from '@testing-library/react';

describe("Parent", () => {

    test("Should render children", () => {
        const { unmount, getByTestId } = render(
            <Parent>
                <BigText text='Child Text' />
            </Parent>
        );
        const child = getByTestId("big-text");
        expect(child.textContent).toMatch(/Child/);
        unmount();
    });

});