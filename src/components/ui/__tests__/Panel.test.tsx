import { describe, test, expect } from 'vitest';
import { render } from '@testing-library/react';
import { Panel } from "../Panel";
import { BigText } from '../BigText';

describe("Panel", () => {

    test("Should render bigText", () => {
        const { unmount, getByTestId } = render(
            <Panel>
                <BigText text='With Panel' />
            </Panel>
        );
        const cmp = getByTestId("big-text");
        expect(cmp.textContent).toMatch(/Panel/);
        unmount();
    });

});
