import { describe, test, expect } from 'vitest';
import { CountButton } from '../CountButton';
import { render, act, fireEvent } from '@testing-library/react';
describe("CountButton", () => {
    test("Should render", () => {
        const { getByTestId, unmount } = render(
            <CountButton value="Count" />
        );
        const cmp = getByTestId("count-button");
        expect(cmp.textContent).toBe("Count = 0 x 0");
        unmount();
    });

    test("Should render 1 if click", () => {
        const { getByTestId, unmount } = render(
            <CountButton value="Value" />
        );
        const cmp = getByTestId("count-button");
        expect(cmp.textContent).toBe("Value = 0 x 0");
        act(() => {
            fireEvent.click(cmp);
        });
        expect(cmp.textContent).toBe("Value = 1 x 0");
        unmount();
    });
});