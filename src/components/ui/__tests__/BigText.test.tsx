import { describe, test, expect } from 'vitest';
import { BigText } from '../BigText';
import { render } from '@testing-library/react';

describe("BigText", () => {

    test("Should render", () => {
        const { getByTestId, unmount } = render(<BigText text="John Woo" />);
        expect(getByTestId('big-text').textContent).toBe("John Woo");
        unmount();
    });

    test("Should render John wick", () => {
        const { getByTestId, unmount } = render(<BigText text="John Wick" />);
        expect(getByTestId('big-text').textContent).toBe("John Wick");
        unmount();
    });

    test("Should render no content", () => {
        const { getByTestId, unmount } = render(<BigText />);
        expect(getByTestId('big-text').textContent).toBe("No Content");
        unmount();
    });

});