import { describe, test, expect } from 'vitest';
import { TitleText } from '../TitleText';
import { render, act, fireEvent } from '@testing-library/react';

describe("TitleText", () => {

    test("Should render", () => {
        const { unmount, getByTestId } = render(<TitleText value='Counter' />);
        const btn = getByTestId("title-text");
        expect(btn.textContent).toBe("Counter 0 x 0");
        unmount();
    });

    test("Should show 1 after click", () => {
        const { unmount, getByTestId } = render(<TitleText value='Value is' />);
        const btn = getByTestId("title-text");
        expect(btn.textContent).toBe("Value is 0 x 0");
        act(() => {
            fireEvent.click(btn);
        });
        expect(btn.textContent).toBe("Value is 1 x 0");
        unmount();
    });

});