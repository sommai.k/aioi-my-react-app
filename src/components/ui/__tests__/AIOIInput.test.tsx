import { describe, test, expect } from 'vitest';
import "@testing-library/jest-dom";
import { render, renderHook } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { AIOIInput } from "../AIOIInput";
import { useForm } from 'react-hook-form';
import { z } from 'zod';

type TestForm = {
    userName: string;
}

const TextOnlySchema = z.object({
    userName: z.string().regex(/[a-z,A-Z]/, "Text Only")
});

type TextOnlyModel = z.infer<typeof TextOnlySchema>;

describe("AIOIInput", () => {
    const user = userEvent.setup();

    test("Should render", async () => {
        const { result } = renderHook(() => useForm<TestForm>());
        const { control } = result.current;
        const { unmount, getByTestId } = render(<AIOIInput label="Username" name="userName" control={control} testid="userName" />);
        // start testcase
        expect(getByTestId('userName-label')).toHaveTextContent("Username");
        expect(getByTestId('userName-input')).toHaveValue("");
        // end testcase
        unmount();
    });

    test("Should input text only", async () => {
        const { result } = renderHook(() => useForm<TextOnlyModel>());
        const { control, getFieldState } = result.current;
        const { unmount, getByTestId } = render(<AIOIInput label="Username" name="userName" control={control} testid="userName" />);
        // start testcase
        await user.type(getByTestId("userName-input"), "abc123");
        expect(getFieldState('userName').invalid).toBeFalsy();
        // end testcase
        unmount();
    });

});