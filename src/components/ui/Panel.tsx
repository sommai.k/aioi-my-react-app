import { FC, ReactNode } from 'react';

interface Props {
    children: ReactNode
}

export const Panel: FC<Props> = (props) => {
    return (
        <div>
            {props.children}
        </div>
    )
}