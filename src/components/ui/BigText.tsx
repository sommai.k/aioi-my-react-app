import { FC } from 'react';

interface Props {
    text?: string;
}

export const BigText: FC<Props> = (props) => {
    return <>
        <div
            data-testid="big-text"
            className='text-2xl'
        >{props.text ?? 'No Content'}</div>
    </>
}