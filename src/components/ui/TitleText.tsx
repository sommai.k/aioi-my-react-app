import { FC, useContext, useState } from 'react';
import { UserContext } from '../../App';

type Props = {
    value: string;
}
export const TitleText: FC<Props> = (props) => {
    const v = useContext(UserContext);
    const [cnt, setCnt] = useState(0);
    const handleClick = () => {
        setCnt(cnt + 1);
    }

    return <button
        data-testid="title-text"
        className='btn-primary'
        onClick={handleClick}>
        {props.value} {cnt} x {v}
    </button>
}

// type State = {
//     cnt: number;
// }

// export class TitleText extends Component<Props, State> {
//     constructor(props: Props) {
//         super(props);
//         this.state = {
//             cnt: 0
//         };
//     }

//     handleClick = () => {
//         this.setState(prevState => ({
//             cnt: prevState.cnt + 1
//         }));
//     }

//     render(): ReactNode {
//         const v = useContext(UserContext);
//         return (
//             <button
//                 data-testid="title-text"
//                 className='btn-primary'
//                 onClick={this.handleClick}>
//                 {this.props.value} {this.state.cnt} x {v}
//             </button>
//         );
//     }
// }
