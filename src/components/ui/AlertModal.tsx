import { FC } from 'react';

interface Props {
    type: 'error' | 'warning' | 'success',
    title: string;
    message: string;
    whenClose: () => void;
    isShow: boolean;
    testid?: string;
}

export const AlertModal: FC<Props> = (props) => {
    return <>
        {props.isShow &&
            <div className='absolute top-0 left-0 w-full h-full flex flex-col space-y-4 z-10 bg-gray-500 bg-opacity-60 items-center justify-center'>
                <div
                    data-testid={props.testid ?? "notestid"}
                    className='flex flex-col border border-gray-200 shadow-lg bg-white min-w-80 min-h-60 p-4'>
                    <div
                        data-testid={`${props.testid}-title` ?? "notestid-title"}
                        className='text-2xl font-bold'
                    >{props.title}</div>
                    <div
                        data-testid={`${props.testid}-message` ?? "notestid-title"}
                        className='flex-1'
                    >{props.message}</div>
                    <div>
                        <button
                            className='btn-primary'
                            data-testid={`${props.testid}-btn` ?? "notestid-btn"} onClick={props.whenClose}>Close</button>
                    </div>
                </div>
            </div>
        }
    </>
}