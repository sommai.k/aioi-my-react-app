import { FC } from 'react';

interface Props {
    isShow: boolean
}

export const AIOIProgress: FC<Props> = (props) => {
    return <>
        {props.isShow &&
            <div className='absolute top-0 left-0 w-full h-full flex flex-col space-y-4 z-10 bg-gray-500 bg-opacity-60 items-center justify-center'>
                <div className="loader"></div>
            </div>
        }
    </>
}