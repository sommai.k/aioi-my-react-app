import { FC, useState, useEffect, useContext } from 'react'
import { UserContext } from '../../App';

interface Props {
    value: string
}

export const CountButton: FC<Props> = (props) => {
    const [count, setCount] = useState<number>(0);
    const v = useContext(UserContext);
    const btnPress = () => setCount((count) => count + 1);
    const display = () => `${props.value} = ${count} x ${v}`;

    useEffect(() => {

        // your logic here
        console.log(props.value);
    });

    return (
        <button
            data-testid="count-button"
            className='btn-primary'
            onClick={btnPress}>{display()}</button>
    );
}