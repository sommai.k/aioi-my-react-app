import { FC } from 'react';
// import { Link } from 'react-router-dom';

interface BreadCrumbProps {
    pageDetail: string;
    pageTitle: string;
}

export const BreadCrumb: FC<BreadCrumbProps> = () => {
    // Inside your component
    return (
        <>
            <span className="d-flex align-items-center">
                <span className="text-start ms-xl-2 mr-1">
                    <span className="d-none d-xl-inline-block ms-1 fw-medium ">
                        <div className="app-search d-none d-md-block">
                            <div className="position-relative">
                                <ol className="breadcrumb m-0">
                                    {/* <li className="breadcrumb-item"><Link to="#"><i className="bx bxs-home"></i></Link></li>
                                    {
                                        props.pageTitle != "" && props.pageTitle != null && (
                                            <li className="breadcrumb-item"><Link to="#">{props.pageTitle}</Link></li>
                                        )
                                    }
                                    {
                                        props.pageDetail != "" && props.pageDetail != null && (
                                            <li className="breadcrumb-item active">{props.pageDetail}</li>
                                        )
                                    } */}
                                </ol>
                            </div>
                        </div>
                    </span>
                </span>
            </span>
        </>
    );
};

// export default BreadCrumb;