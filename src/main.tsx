import React from 'react'
import ReactDOM from 'react-dom/client'
import './index.css'

import { Provider } from 'react-redux';
import { setupStore } from './app/store.ts';

import { RouterProvider } from 'react-router-dom';
import { router } from './router.tsx';

// import { setupWorker } from 'msw/browser'
// import { handlers } from '../utils/mocks/handlers';

// const worker = setupWorker(
//   ...handlers
// )

// end mock api server

const store = setupStore();
ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <Provider store={store}>
      <RouterProvider router={router} />
    </Provider>
  </React.StrictMode>,
)
