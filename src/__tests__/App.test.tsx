import { describe, test, expect } from 'vitest';
import { render, act } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { App } from '../App';

describe("App", () => {

    test("Should render", () => {
        const { getByTestId, unmount } = render(<App />);
        const titleText = getByTestId("title-text");
        expect(titleText.textContent).toBe("T = 0 x 0")
        unmount();
    });

    test("Should render x = 1", async () => {
        const user = userEvent.setup();
        const { getByTestId, getByText, unmount } = render(<App />);
        const titleText = getByTestId("title-text");
        const btn2 = getByText("Test");
        const countBtn = getByTestId("count-button");

        await act(() => user.click(btn2));
        expect(titleText.textContent).toBe("T = 0 x 1");
        expect(countBtn.textContent).toBe("Count = 0 x 1");

        await act(() => user.click(btn2));
        expect(titleText.textContent).toBe("T = 0 x 2");
        expect(countBtn.textContent).toBe("Count = 0 x 2");
        unmount();
    });

});