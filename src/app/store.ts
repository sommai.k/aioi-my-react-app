import { combineReducers, configureStore } from '@reduxjs/toolkit'
import { authReducer } from './features/auth-slice';

const rootReducer = combineReducers({
    // reducer
    authReducer,
});

export const setupStore = (preloadedState?: Partial<RootState>) => {
    return configureStore({
        reducer: rootReducer,
        preloadedState
    })
}

export type RootState = ReturnType<typeof rootReducer>
export type AppStore = ReturnType<typeof setupStore>
export type AppDispatch = AppStore['dispatch']