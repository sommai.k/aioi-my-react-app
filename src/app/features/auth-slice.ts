import { PayloadAction, createSlice } from '@reduxjs/toolkit';
import { api } from '../api';

interface Authen {
    token: string;
    firstName: string;
    lastName: string;
    userName: string;
}

interface AuthState extends Authen {
    isLogedIn: boolean;
}

const initialState: AuthState = {
    token: '',
    isLogedIn: false,
    firstName: '',
    lastName: '',
    userName: ''
}

const authSlice = createSlice({
    name: 'auth',
    initialState,
    reducers: {
        login: (_, action: PayloadAction<Authen>) => {
            api.defaults.headers.common.Authorization = `Bearer ${action.payload.token}`;
            localStorage.setItem('auth', JSON.stringify({ isLogedIn: true, ...action.payload }));
            return { isLogedIn: true, ...action.payload }
        },
        logout: () => {
            api.defaults.headers.common.Authorization = null;
            localStorage.removeItem('auth');
            return initialState;
        }
    }
});

export const { login, logout } = authSlice.actions;
export const authReducer = authSlice.reducer;