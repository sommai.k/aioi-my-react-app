import axios from 'axios';

const api = axios.create();
api.defaults.baseURL = "";
api.defaults.responseEncoding = "application/json";

export { api };