import { useState } from 'react';

export const useProgress = () => {
    const [isShow, setIsShow] = useState(false);

    const show = () => {
        setIsShow(true);
    }

    const hide = () => {
        setIsShow(false);
    }

    return { isShow, show, hide }
}