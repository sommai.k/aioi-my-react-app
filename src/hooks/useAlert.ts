import { useState } from 'react';

interface AlertModel {
    title: string;
    message: string;
    isShow: boolean;
}

const alertInitState: AlertModel = { title: '', message: '', isShow: false };

export const useAlert = () => {
    const [alert, setAlert] = useState<AlertModel>(alertInitState);

    const warning = (message: string) => {
        setAlert({ title: 'Warning', message, isShow: true });
    }

    const error = (message: string) => {
        setAlert({ title: 'Error', message, isShow: true });
    }

    const info = (message: string) => {
        setAlert({ title: 'Info', message, isShow: true });
    }

    const hide = () => {
        setAlert(alertInitState);
    }
    const model = alert;
    return { model, warning, error, info, hide }
}