import { useState } from "react";

interface ConfirmModel {
    title: string;
    message: string;
    isShow: boolean;
    whenOk?: () => void;
    whenCancel?: () => void;
}
const confirmInitState: ConfirmModel = { title: '', message: '', isShow: false };

export const useConfirm = () => {

    const [alert, setAlert] = useState<ConfirmModel>(confirmInitState);

    const show = (title: string, message: string, whenOk: () => void, whenCancel: () => void) => {
        setAlert({ title, message, isShow: true, whenOk, whenCancel });
    }

    const hide = () => {
        setAlert(confirmInitState);
    }

    const model = alert;
    return { model, show, hide }

}