export interface UserResp {
    success: boolean;
    data: [],
    total: number;
}