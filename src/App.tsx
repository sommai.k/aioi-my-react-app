import { FC, createContext, useState } from 'react';

export const UserContext = createContext(0);

export const App: FC = () => {
  const [value] = useState(0);

  // custom function
  const whenButtonPress = async () => {
    try {
      const resp = await fetch("/api/birds/about");
      const data = await resp.json();
      console.log(data);
    } catch (e) {
      console.log(e);
    }
  }

  return <>
    <UserContext.Provider value={value}>
      <button className='btn-primary' onClick={whenButtonPress}>Test Fetch</button>
    </UserContext.Provider>
  </>
}