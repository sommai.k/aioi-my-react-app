FROM node:20-alpine AS build-env

WORKDIR /app/
COPY . /app/
RUN npm i -g npm
RUN npm i
RUN npm run build

FROM nginx:alpine
ENV API_URL=http://api:3000/
RUN mkdir /etc/nginx/templates
COPY ./nginx /etc/nginx/templates
COPY --from=build-env /app/dist /usr/share/nginx/html
