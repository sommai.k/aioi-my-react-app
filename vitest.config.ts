import react from '@vitejs/plugin-react'
import { defineConfig } from 'vitest/config'

// https://vitejs.dev/config/
export default defineConfig({
    plugins: [react()],
    test: {
        root: "./src",
        globals: true,
        environment: 'jsdom',
        coverage: {
            provider: 'v8',
            reportsDirectory: '../coverage',
            exclude: ["main.tsx"]
        },
        // setupFiles: './utils/setupTest.js'
        // css: true,
    },
})
